# Laravel Staticize View

Laravel 静态化视图组件。需要 Nginx 配合。Nginx 配置参考 `nginx.conf.example` 文件。

## 常规视图静态化

在需要生成静态化的控制器方法中使用 `staticize_view()` 函数。例如

```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return staticize_view('home');
    }
}
```

生成的静态文件位于 `storage/app/staticize_view` 目录。

## API 接口静态化

在需要生成静态化的控制器方法中使用 `staticize_json()` 函数。例如

```php
<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return staticize_json([
          'status' => 'ok',
        ]);
    }
}
```

## 禁用静态化内容生成功能

在 `.env` 配置中添加如下配置项:

```ini
STATICIZE_VIEW_DISABLED=true
```

## 删除所有生成的静态化内容

执行如下命令:

```
artisan staticize-view:clear
```

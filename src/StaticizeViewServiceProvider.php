<?php

namespace ZzxWorld\LaravelStaticizeView;

use Illuminate\Support\ServiceProvider;

class StaticizeViewServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/staticize_view.php', 'staticize_view');
    }

    public function boot()
    {
        $this->publishes([
            __DIR__.'/staticize_view.php' => config_path('staticize_view.php'),
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                StaticizeViewClear::class,
            ]);
        }
    }
}

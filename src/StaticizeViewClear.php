<?php

namespace ZzxWorld\LaravelStaticizeView;

use Illuminate\Console\Command;
use Storage;

class StaticizeViewClear extends Command
{
    protected $signature = 'staticize-view:clear';
    protected $description = 'Clear staticize view render cache folder.';

    public function handle()
    {
        Storage::disk('local')->deleteDirectory(staticize_view_path());

        $this->info('Staticize views cached cleared!');
    }
}
